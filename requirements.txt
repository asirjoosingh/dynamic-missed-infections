bokeh==1.4.0
click==7.1.2
cycler==0.10.0
Flask==1.1.2
itsdangerous==1.1.0
Jinja2==2.11.2
kiwisolver==1.2.0
MarkupSafe==1.1.1
matplotlib==3.2.2
numpy==1.19.0
packaging==20.4
pandas==1.0.5
patsy==0.5.1
Pillow==7.2.0
pyparsing==2.4.7
python-dateutil==2.8.1
pytz==2020.1
PyYAML==5.3.1
scipy==1.5.1
seaborn==0.10.1
six==1.15.0
statsmodels==0.11.1
tornado==6.0.4
Werkzeug==1.0.1
xlrd==1.2.0
