# Interactive Scientific Letter

An Rmarkdown template for generating scientific letters in a format that can be shared on the web.

You can include interactive features like R Shiny or Python bokeh applications within your letter to add interactivity.

## Getting Started

### Get the template
Make a bare clone of this repository by running:

```
git clone --bare git@10.254.1.5:triffm/interactive-scientific-letter.git
```

### Create your letter

Create a new Rmd file in the root of the cloned repository (or rename and start editing the existing `template.Rmd` file).

### Generate the HTML file

After you have finished making changes to the Rmd file, you can generate your HTML file through either of two methods:

1. From the command-line, run: `R -e "rmarkdown::render('YOUR-RMARKDOWN-FILENAME.Rmd')"` from the root of the repository.
2. Open `YOUR-RMARKDOWN-FILENAME.Rmd` in R Studio and click the "Knit" button.

This will generate the HTML version of your letter with the name `YOUR-RMARKDOWN-FILENAME.html`.

## Hosting the Letter

Most files are automatically included in the output HTML file, however, some files need to be hosted separately:

```
static/images/dia-logo.png
static/images/drdc-logo.jpg
```

## Interactive Components

### Including interactive applications
Interactive components can be included in your letter using either the `knitr::include_app()` (for R Shiny applications) or `knitr::include_url()` (for all other applications).

Since applications are loaded in an iFrame, you must host them on the same domain as the scientific letter for them to appear in all browsers.

### Sizing interactive applications correctly

To ensure your letter correctly resizes for all screen sizes and devices, you **must** include the `static/client/iframeResizer.contentWindow.min.js` into your applications.

For example, in an R Shiny application, copy the `iframeResizer.contentWindow.min.js` file into your `www` folder and include it in your UI with:
```
    includeScript("www/iframeResizer.contentWindow.min.js")
```
