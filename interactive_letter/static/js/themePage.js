// Helper Functions
function wrapInColumn(element, columnClass) {
  var colWrapper = document.createElement('div');
  if (columnClass === null || columnClass === undefined) {
    columnClass = 'col';
  }
  colWrapper.className = columnClass;
  if (element instanceof Array) {
    element[0].parentNode.insertBefore(colWrapper, element[0]);
    for (var i = 0; i < element.length; i++) {
      colWrapper.appendChild(element[i]);
    }
  } else {
    element.parentNode.insertBefore(colWrapper, element);
    colWrapper.appendChild(element);
  }
  return colWrapper;
}

function wrapInRow(element) {
  var rowWrapper = document.createElement('div');
  rowWrapper.className = 'row';
  element.parentNode.insertBefore(rowWrapper, element);
  rowWrapper.appendChild(element);
  return rowWrapper;
}

function wrapInRowAndColumn(element, columnClass) {
  var column = wrapInColumn(element, columnClass);
  return wrapInRow(column);
}

// Main Processing
var lang=document.getElementsByTagName("html")[0].getAttribute("lang");

var bodyElements = document.getElementsByTagName('body');
if (bodyElements.length > 0) {
  bodyElements[0].innerHTML = '<div class="container" style="background-color: #FFF">' + bodyElements[0].innerHTML + '</div>';
}

var h1headers = document.getElementsByTagName('h1');
if (h1headers.length > 0) {
  for (var i = 0; i < h1headers.length; i++) {
    wrapInRowAndColumn(h1headers[i]);
  }
  h1headers[0].style.marginTop = '0.5rem';
  h1headers[0].style.color = '#004ca3';
}

var authorHeaders = document.getElementsByClassName('author');
if (authorHeaders.length > 0) {
  var column = wrapInColumn(authorHeaders[0], 'col-md');
  column.style.marginTop = '0.5em';
  var row = wrapInRow(column);
  // Add all other authors to the same row
  for (var i = 1; i < authorHeaders.length; i++) {
    var column = wrapInColumn(authorHeaders[i]);
    row.appendChild(column);
  }
  for (var i = authorHeaders.length - 1; i >= 0; i--) {
    var authorSpan = document.createElement('span');
    authorSpan.innerHTML = authorHeaders[i].innerHTML;
    authorHeaders[i].parentElement.appendChild(authorSpan);
    authorHeaders[i].parentElement.removeChild(authorHeaders[i]);
  }
}

var instituteHeaders = document.getElementsByClassName('institute');
if (instituteHeaders.length > 0) {
  var column = wrapInColumn(instituteHeaders[0], 'col-md');
  column.style.marginTop = '0.5em';
  // Add all other institutions to the same row
  for (var i = 1; i < instituteHeaders.length; i++) {
    column.appendChild(instituteHeaders[i]);
  }
}

var headerElement = document.getElementsByTagName('header');
var divider = document.createElement('hr');
divider.className = 'my-4';
headerElement[0].insertAdjacentElement('beforeend', divider);
wrapInRowAndColumn(divider);

// wrap paragraphs between headers into rows
var header = document.getElementById('header');
if (header != null) {
  var introElements = $('#header').nextUntil('.section').toArray();
  if (introElements != undefined && introElements != null && introElements.length > 0) {
    wrapInRowAndColumn(introElements);
  }
}

var sections = document.getElementsByClassName('section');
if (sections.length > 0) {
  for (var i = 0; i < sections.length; i++) {
    var sectionId = sections[i].id;
    // var selector = '#' + sectionId + ' :not(:header)';
    var section = document.getElementById(sectionId);
    var sectionChildren = [];
    for (var j = 0; j < section.childNodes.length; j++) {
      // if (!$(section.childNodes[j]).is(':header')) {
        sectionChildren.push(section.childNodes[j]);
      // }
    }
    if (sectionChildren !== undefined && sectionChildren !== null && sectionChildren.length > 0) {
      var row = wrapInRowAndColumn(sectionChildren);
      row.style.display = 'block';
    }
  }
}

// convert first ul to highlights
var ulItems = document.getElementsByTagName('ul');
if (ulItems.length > 0) {
  var jumbotron = document.createElement('div');
  jumbotron.className = 'jumbotron py-4';
  ulItems[0].insertAdjacentElement('afterend', jumbotron);
  var highlightsHeader = document.createElement('h4');
  highlightsHeader.style.color = '#004ca3';
  highlightsHeader.style.fontSize = 'x-large';
  if (lang == "fr") {
    highlightsHeader.innerText = 'Principaux points';
  }
  else {
    highlightsHeader.innerText = 'Key Points';
  }
  jumbotron.appendChild(highlightsHeader);
  var divider = document.createElement('hr');
  divider.class = 'my-3';
  jumbotron.appendChild(divider);
  var liItems = ulItems[0].getElementsByTagName('li');
  for (var i = 0; i < liItems.length; i++) {
    var paragraph = document.createElement('p');
    paragraph.setAttribute('style', 'font-size: medium; font-size: initial;');
    jumbotron.appendChild(paragraph);
    var icon = document.createElement('i');
    icon.className = 'far fa-dot-circle fa-xs';
    icon.style.paddingRight = '0.5em';
    paragraph.appendChild(icon);
    paragraph.innerHTML = paragraph.innerHTML + liItems[i].innerHTML;
  }
  ulItems[0].parentElement.removeChild(ulItems[0]);

  var head = document.getElementsByTagName('head');
  if (head.length != 0) {
    var highlightStyle = document.createElement('style');
    highlightStyle.type = 'text/css';
    highlightStyle.innerText = '@media screen and (min-width: 768px){ .jumbotron { max-width: 48%; float: right; margin-left: 1em }}';
    head[0].appendChild(highlightStyle);
  }
}

if (lang == "fr") {
  var references = document.getElementById('références');
}
else {
  var references = document.getElementById('references');
}
var referenceLists = references.getElementsByTagName('ol');
if (referenceLists.length > 0) {
  referenceLists[0].style.wordWrap = 'break-word';
}

// add logos
var containers = document.getElementsByClassName("container");
if (containers.length > 0) {
  var drdcLogo = document.createElement('img');
  drdcLogo.src = 'static/images/drdc-logo.jpg';
  drdcLogo.style.maxHeight = '100%'; 
  drdcLogo.style.float = 'right';

  containers[0].insertAdjacentElement('afterbegin', drdcLogo);
  var logoColumn = wrapInColumn(drdcLogo, 'col text-right');
  logoColumn.style.height = '10vh';
  
  var diaLogo = document.createElement('img');
  diaLogo.src = 'static/images/dia-logo.png';
  diaLogo.style.maxHeight = '100%'; 
  diaLogo.style.float = 'right';
  
  logoColumn.insertAdjacentElement('afterbegin', diaLogo);
  var row = wrapInRow(logoColumn);
  row.style.marginTop = '0.75em';

  var langlink=document.getElementsByClassName("langLink");
  if (langlink.length > 0) {
    langlink[0].style.fontSize = '1em';
    langlink[0].style.textDecoration = 'underline';
    if (lang != 'en') {
      langlink[0].innerHTML = 'English';
    } else {
      langlink[0].innerHTML = 'Fran&ccedil;ais';
    }
    var langlinkColumn = wrapInColumn(langlink[0], 'col text-left');
    row.insertAdjacentElement('afterbegin', langlinkColumn);
    row.style.display = 'flex';
    row.style.alignItems = 'center';
  }
}

// Add classification tag
var classificationTopHeader = document.createElement('h5');
if (lang == "fr") {
  classificationTopHeader.innerText = 'DOCUMENT NON PROTÉGÉ';
}
else {
  classificationTopHeader.innerText = 'UNCLASSIFIED';
}
var classificationTopDiv = document.createElement('div');
classificationTopDiv.appendChild(classificationTopHeader);
classificationTopDiv.classList.add('col')
classificationTopDiv.classList.add('text-center');
classificationTopDiv.style.position = 'absolute'; 
classificationTopDiv.style.zIndex = '1';
classificationTopDiv.style.width = '100%';
bodyElements[0].insertAdjacentElement('afterbegin', classificationTopDiv);

var classificationBottomHeader = document.createElement('h5');
if (lang == "fr") {
  classificationBottomHeader.innerText = 'DOCUMENT NON PROTÉGÉ';
}
else {
  classificationBottomHeader.innerText = 'UNCLASSIFIED';
}
var classificationBottomDiv = document.createElement('div');
classificationBottomDiv.appendChild(classificationBottomHeader);
classificationBottomDiv.classList.add('col');
classificationBottomDiv.classList.add('text-center');
containers[0].appendChild(classificationBottomDiv);

// Add iFrame resizing if necessary
var iframes = document.getElementsByTagName('iframe');
if (iframes.length > 0) {
  for (var i = 0; i < iframes.length; i++) {
    iframes[i].style.width = '100%';
    iFrameResize({}, iframes[i]);
  }
}
