
import numpy as np
import pandas as pd
from urllib.request import urlopen
from urllib.parse import quote

def getpointprevalence(hr):
    """ Get point prevalence values from the COVID-19 Point-Prevalence Estimation Map on Gitlab.

    The health region `hr` must be specified in full and match the existing directory
    structure on the Gitlab repository for the point prevalence tool (in results/latest/).
    The current data file in the repository will be parsed for the  median prevalence
    (`pointprevalence_50` column) and the latest value will be returned (defined as the day before `now`).
    The repository files are accessed using the Gitlab API with the covid-prevalence repository id=19341877
    If there is an error in retrieving the value, -1 is returned.

    Inputs:
      hr (str)       : name of the health region matching the Gitlab directory structure

    Returns:
      latestprev (float) : most recent median value of point prevalence for health region
    """
    baseurl="https://gitlab.com/api/v4/projects/21984746/repository/files/results%2Flatest%2F"
    url=baseurl+quote(hr)+"%2F"+quote(hr)+"_timeseries%2Ecsv"+"/raw?ref=master" # quote hr for French accent conversion
    try:
        with urlopen(url) as f:
          df=pd.read_csv(f,header=0,index_col=None,usecols=["dates","pointprevalence_50"])
        now=pd.Timestamp.now().normalize()
        df["dates"]=pd.to_datetime(df["dates"])
        df=df[np.logical_and(df["dates"]<now,pd.notna(df["pointprevalence_50"]))]
        latestprev=float(df.iloc[-1,1])
    except:
        latestprev=-1
    return latestprev

