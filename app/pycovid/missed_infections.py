
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy as sp
import scipy.stats
import scipy.integrate
import scipy.signal
import csv


class ConvolvedDistribution:
    """Convolved probability distribution.

    Statistical functions are calculated numerically during initialization and
    interpolated upon evaluation. The statistical function methods are created
    at initialization; each of them take an array of times as argument and
    returns an array of function values.
    
    Methods:
        __init__ -- constructor
        pdf -- probability density function
        cdf -- cumulative distribution function
        sf -- survival function
        mean -- mean value

    """
    
    def __init__(self, dist1, dist2, t_max, dt):
        """Initialize convolved distribution.
        
        Arguments:
            dist1: first distribution of the convolution
            dist2: second distribution of the convolution
            t_max: maximum time at which to evaluate statistical functions
            dt: time step at which to evaluate statistical functions
        """
        # Calculate statistical functions
        t, dt_actual = np.linspace(0, t_max, round(t_max / dt), retstep=True)
        pdf = sp.signal.fftconvolve(dist1.pdf(t),
                                    dist2.pdf(t))[:len(t)] * dt_actual
        # Round small values to zero, with small being defined as the absolute
        # value of the most negative number.
        pdf[pdf < abs(pdf.min())] = 0
        cdf = np.cumsum(pdf) * dt_actual
        sf = 1 - cdf
        # Make sure that CDF does not exceed one or SF go below zero because of
        # rounding errors.
        cdf.clip(max=1)
        sf.clip(min=0)
        # Create interpolation functions
        self._pdf = sp.interpolate.interp1d(t, pdf, assume_sorted=True)
        self._cdf = sp.interpolate.interp1d(t, cdf, assume_sorted=True)
        self._ppf = sp.interpolate.interp1d(cdf, t, assume_sorted=True)
        self._sf = sp.interpolate.interp1d(t, sf, assume_sorted=True)
        # Statistics
        self._mean = sp.integrate.trapz(pdf * t, dx=dt_actual)

    def pdf(self, t):
        """Evaluate probablity density function.
        
        Argument:
            t -- time at which to evaluate function
            
        Returns:
            Probability density
        """
        return self._pdf(t)

    def cdf(self, t):
        """Evaluate cumulative distribution function.
        
        Argument:
            t -- time at which to evaluate function
            
        Returns:
            Probability
        """
        return self._cdf(t)

    def sf(self, t):
        """Evaluate survival function.
        
        Argument:
            t -- time at which to evaluate function
            
        Returns:
            Probability
        """
        return self._sf(t)

    def ppf(self, p):
        """Evaluate percent point function (inverse of CDF).
        
        Argument:
            p -- probability at which to evaluate function
            
        Returns:
            Value for which the cumulative distribution function is p
        """
        return self._ppf(t)

    def rvs(self, size):
        """Generate random variates.
        
        Argument:
            size -- number of random variables to generate
            
        Returns:
            Sequence of random numbers
        """
        return self.ppf(np.random.uniform(size=size))

    def mean(self):
        """Evaluate mean.

        Returns:
            Mean value
        """
        return self._mean


# Upper bound and time step for integrals.
t_infinite = 1000
dt = 0.1


# Duration of presymptomatic phase.
mean_pre, stddev_pre = 1.621, 0.418 # mean and standard deviation
presymptomatic = sp.stats.lognorm(stddev_pre, scale=np.exp(mean_pre))


# Duration of symptoms for mild symptomatic case.
mean_sym, stddev_sym = 10.0, 3.0
scale_sym = stddev_sym ** 2 / mean_sym
shape_sym = mean_sym / scale_sym
symptomatic_mild = sp.stats.gamma(shape_sym, scale=scale_sym)


# Duration of infectious phase for mild symptomatic cases.
mean_inf, stddev_inf = 4.59, 1.74
scale_inf = stddev_inf ** 2 / mean_inf
shape_inf = mean_inf / scale_inf
infectious_mild = sp.stats.gamma(shape_inf, scale=scale_inf)


# Duration of symptomatic case, modelled as a convolution of the distributions
# for the duration of the presymptomatic phase and the duration of symptoms for
# mild symptomatic cases. (Neglects severe and critical cases.)
symptomatic = ConvolvedDistribution(presymptomatic, symptomatic_mild, 
                                    t_infinite, dt)


# Duration of asymptomatic case, assumed to be equal to the duration of mild
# symptomatic cases.
asymptomatic = ConvolvedDistribution(presymptomatic, infectious_mild, 
                                     t_infinite, dt)


def p_nonsymptomatic(t_exposure, t_sequestration, dt, sf_num, sf_den):
    """Calculate probability of being non-symptomatic and unresolved.

    The probability is calculated conditional on being already infected. Used as
    calculation engine by p_unresolved_asymptomatic and p_presymptomatic
    functions.

    Arguments:
        t_exposure -- number of days of exposure before sequestration (must be
            non-negative)
        t_sequestration -- number of days of sequestration
        dt -- time step used for integration
        sf_num -- survival function integrated in numerator
        sf_den -- survival function integrated in denominator

    Returns:
        Probability (scalar value)
    """
    if t_exposure < 0:
        raise ValueError('t_exposure must be positive')
    elif t_exposure == 0:
        return 0
    else:
        t_num, dt_num = np.linspace(t_sequestration,
                                    t_sequestration + t_exposure,
                                    round(t_exposure / dt), retstep=True)
        t_den, dt_den = np.linspace(0, t_infinite,
                                    round(t_infinite / dt), retstep=True)
        return (sp.integrate.trapz(sf_num(t_num), dx=dt_num)
                / sp.integrate.trapz(sf_den(t_den), dx=dt_den))


def p_unresolved_asymptomatic(t_exposure, t_sequestration, dt):
    """Calculate probability of an asymptomatic case being still unresolved.

    Arguments:
        t_exposure -- number of days of exposure before sequestration
        t_sequestration -- number of days of sequestration
        dt -- time step used for integration

    Returns:
        Probability (scalar value)
    """
    return p_nonsymptomatic(t_exposure, t_sequestration, dt,
                            asymptomatic.sf, asymptomatic.sf)


def p_presymptomatic(t_exposure, t_sequestration, dt):
    """Calculate probability of a symptomatic case being still pre-symptomatic.

    Arguments:
        t_exposure -- number of days of exposure before sequestration
        t_sequestration -- number of days of sequestration
        dt -- time step used for integration

    Returns:
        Probability (scalar value)
    """
    return p_nonsymptomatic(t_exposure, t_sequestration, dt,
                            presymptomatic.sf, symptomatic.sf)


def p_missed_given_infected(p_asymptomatic, p_false_negative, n_PCR,
                            t_exposure, t_sequestration, dt):
    """Calculate probability of an infected person being undetected.

    This function calculates the probability of an infected person being
    non-symptomatic and undetected after n_PCR PCR tests and t_sequestration
    days of sequestration following t_exposure days of potential exposure.

    Arguments:
        p_asymptomatic -- probability of an infected person being asymptomatic
        p_false_negative -- probability of false negative in a PCR test
        n_PCR -- number of PCR tests administered
        t_exposure -- number of days of exposure before sequestration
        t_sequestration -- number of days of sequestration
        dt -- time step used for integration

    Returns:
        Probability (scalar value)
    """
    mean = (p_asymptomatic * asymptomatic.mean()
            + (1 - p_asymptomatic) * symptomatic.mean())
    return (p_false_negative ** n_PCR
            * (p_asymptomatic * asymptomatic.mean() / mean
                 * p_unresolved_asymptomatic(t_exposure, t_sequestration, dt)
               + (1 - p_asymptomatic) * symptomatic.mean() / mean
                 * p_presymptomatic(t_exposure, t_sequestration, dt)))


def missed_infections(n_persons, p_infected, p_asymptomatic, p_false_negative,
                      n_PCR, t_exposure, t_sequestration, dt):
    """Return distribution of number of missed infections in group.

    Arguments:
        n_persons -- number of persons in the group
        p_infected -- probability of random person being infected (point
            prevalence)
        p_asymptomatic -- probability of an infected person being asymptomatic
        p_false_negative -- probability of false negative in a PCR test
        n_PCR -- number of PCR tests administered
        t_exposure -- number of days of exposure before sequestration
        t_sequestration -- number of days of sequestration
        dt -- time step used for integration

    Return:
        scipy.stats distribution object
    """
    p_undetected = p_missed_given_infected(p_asymptomatic, p_false_negative, n_PCR,
                                       t_exposure, t_sequestration, dt)
    dist = sp.stats.binom(n_persons, p_infected * p_undetected)
    return dist


def round_significant(value, digits):
    """Round value to specified number of significant digits."""
    return round(value, -int(np.floor(np.log10(value)) - digits + 1))


def print_table():
    """Save results for CFHS COVID-19 testing placemat."""
    # p_a: probability of an infected person being asymptomatic
    # p_fn: probability of false negative
    p_a = 0.2
    p_fn = 0.3
    group_sizes = (10, 25, 50, 100, 250, 500, 1000)
    prevalences = (0.001, 0.01)
    fieldnames = ['Tier', 'Prevalence', 'Days of quarantine', '# of tests']
    fieldname_prob = 'Prob of 1+ for %i (%%)'
    fieldnames.extend([fieldname_prob % gs for gs in group_sizes])
    rows = [('1 and 2', 14, 0), ('1 and 2', 14, 1), ('1 and 2', 14, 2),
            ('1 and 2', 14, 3),
            ('1 and 2', 10, 0), ('1 and 2', 10, 1), ('1 and 2', 10, 2),
            ('1 and 2', 7, 0), ('1 and 2', 7, 1), ('1 and 2', 7, 2),
            ('1 and 2', 0, 0), ('1 and 2', 0, 1),
            ('3', 14, 0), ('3', 14, 1), ('3', 0, 0), ('3', 0, 1)]
    with open('prob_missed_case.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        # p_i: probability of a random person being infected (point prevalence)
        for p_i in prevalences:
            for (tier, t_seq, n_PCR) in rows:
                row = {'Tier': tier,
                       'Prevalence': p_i,
                       'Days of quarantine': t_seq,
                       '# of tests': n_PCR}
                for gs in group_sizes:
                    dist = missed_infections(gs, p_i, p_a, p_fn,
                                             n_PCR, 950, t_seq, dt)
                    row[fieldname_prob % gs] \
                            = round_significant(100 * dist.sf(0), 1)
                writer.writerow(row)


def plot_group_with_test(n_persons, y_lim, xy_ann, xytext_ann):
    """Plot probability of undetected infection in group."""
    # p_i: probability of a random person being infected (point prevalence)
    # p_a: probability of an infected person being asymptomatic
    # p_fn: probability of false negative
    # n_PCR: number of PCR tests per person
    # t_exp: number of days of potential exposure (set here to a large value to
    #        assume equilibrium with community prevalence)
    # t_seq: days of quarantining (sequestration)
    # dt: time step used for integration
    p_i = 0.01
    p_a = 0.2
    p_fn = 0.3
    t_exp = 950
    t_seq = np.linspace(0, 14, 51)
    figures, axes = plt.subplots(figsize=(9, 5))
    axes.set_title('Probability of missing at least one infected case\n'
                   'in a group of %i with a 1%% prevalence ' % n_persons,
                   size='x-large', y=1.02)
    axes.set_yscale('log')
    axes.set_ylim(*y_lim)
    axes.set_xlabel('Days of quarantine', size='large')
    axes.set_ylabel('Probability (%)', size='large')
    formatter = ticker.FuncFormatter(lambda y, _: '%.16g' % y)
    axes.yaxis.set_major_formatter(formatter)
    # n_PCR: number of PCR tests per person
    for n_PCR in range(4):
        p =  [100 * missed_infections(n_persons, p_i, p_a, p_fn,
                                      n_PCR, t_exp, t, dt).sf(0)
              for t in t_seq]
        axes.plot(t_seq, p, label='%i Tests' % n_PCR)
        axes.annotate(n_PCR, xy=(t_seq[0], p[0]), va='center', ha='right',
                      xytext=(-3, 0), textcoords='offset points')
    axes.annotate("# of tests per person", xy=xy_ann, va='center', ha='left',
                  xytext=xytext_ann, textcoords='offset points',
                  arrowprops=dict(arrowstyle='->'))
    plt.savefig('prob_missed_case.pdf', transparent=False, format='pdf')


def plot_t_exposure(p_i):
    """Plot probability of undetected infection in group."""
    p_a = 0.2
    p_fn = 0.3
    t_exp = np.linspace(0, 16, 61)
    t_seq = 0
    dt = 0.1
    n_PCR = 0
    figure, axes = plt.subplots(figsize=(9,5))
    axes.set_title('Probability of a person being non-symptomatic\n'
                   '(%g%% prevalence, no quarantining, no test)\n' % (100 * p_i),
                   size=14, y=1.02)
    axes.set_xlabel('Days in community', size='large')
    axes.set_ylabel('Probability (%)', size='large')
    p = [100 * missed_infections(1, p_i, p_a, p_fn, n_PCR, t, t_seq, dt).sf(0)
         for t in t_exp]
    axes.plot(t_exp, p)
    plt.grid()
    plt.tight_layout()
    plt.savefig('prob_vs_t_exposure_%g%%.pdf' % (100 * p_i), transparent=False, format='pdf')


def plot_t_exposure_and_quarantine():
    """Plot exposure and quaranting reduction factor for asymptomatic cases."""
    t_exp = np.linspace(0, 15, 61)
    t_seq = np.linspace(0, 15, 61)
    dt = 0.01
    figure, axes = plt.subplots(figsize=(3,3))
    p1 = [p_unresolved_asymptomatic(t, t_seq[0], dt) for t in t_exp]
    p2 = [p_unresolved_asymptomatic(t_exp[-1], t, dt) for t in t_exp]
    #axes.plot(t_exp.tolist() + (t_seq + t_exp[-1]).tolist(), p1 + p2)
    axes.plot(t_exp.tolist(), p1, color='red')
    axes.plot(t_seq + t_exp[-1], p2, color='#00B050')
    axes.set_xlim(0, t_exp[-1] + t_seq[-1])
    axes.set_ylim(0, 1)
    plt.axvline(x=t_exp[-1], color='black', linestyle=':', linewidth=1)
    plt.grid()
    plt.tight_layout()
    plt.savefig('prob_vs_t_exposure_and_quarantine.pdf', transparent=False, format='pdf')


def main():
    print_table()
    plot_group_with_test(100, (0.01, 100), (-0.1, 1.2), (0, -35))
    #plot_t_exposure(0.01)
    #plot_t_exposure(0.001)
    #plot_t_exposure_and_quarantine()


if __name__ == '__main__':
    main()
