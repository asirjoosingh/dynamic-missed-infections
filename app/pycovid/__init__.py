
"""Probability of missing a COVID 19 case after quarantining and testing.

Calculates the probability of missing an infected COVID-19 case in a group
after quarantining and testing. Results are saved in a CSV and PDF files.

Assumptions:

 1. During isolation, individuals are kept away from each other, so they cannot
    cross-contaminate. If they are not isolated from each other, this analysis
    is likely to significantly underestimate the number of undetected cases.

 2. Individuals with COVID-19 symptoms are isolated, whether or not their 
    PCR test result is positive or not.

 3. Point prevalence is constant.

 4. A fraction of individuals who catch COVID-19 remain asymptomatic despite 
    being contagious.

 5. The PCR test has a constant false negative rate for asymptomatic and
    pre-symptomatic cases.

 6. Getting a false negative in a PCR test does not increase the risk of 
    false negative in subsequent tests.

 7. Infected individuals have a pre-symptomatic phase, the duration of which 
    follows a log-normal distribution.

 8. The symptomatic phase for mild cases follows a gamma distribution.

 9. The infectious phase for mild symptomatic cases follows a gamma
    distribution.

10. Asymptomatic cases last as long as the pre-symptomatic and infectious phases 
    of mild symptomatic cases.

Constants:
    t_infinite -- large value of time used as upper bound for integrals to
        infinity
    dt -- integration time step
    mean_pre and stddev_pre -- mean and standard deviation of the normal
        distribution underlying the log-normal distribution for the duration of
        the pre-symptomatic phase
    mean_sym, stddev_sym, shape_sym and scale_sym -- mean, standard deviation,
        shape parameter and scale parameter of the gamma distribution
        describing the duration of the symptomatic phase for mild symptomatic
        cases
    mean_inf, stddev_inf, shape_inf and scale_inf -- mean, standard deviation,
        shape parameter and scale parameter of the gamma distribution
        describing the duration of the infectious phase for mild symptomatic
        cases

Probability distributions:
    presymptomatic -- duration of presymptomatic phase, modelled as log-normal
        distribution
    symptomatic_mild -- duration of symptoms for mild symptomatic case,
        modelled as a gamma distribution
    infectious_mild -- duration of infectious phase for mild symptomatic cases,
        modelled as a gamma distribution
    symptomatic -- duration of symptomatic cases, modelled as the convolution
        of the distributions for the duration of the presymptomatic phase and
        the duration of symptoms for mild symptomatic cases (neglects severe
        and critical cases.)
    asymptomatic -- duration of asymptomatic cases, modelled as the convolution
        of the distributions for the duration of the presymptomatic phase and
        the duration of infectiousness for mild symptomatic cases

Classes:
    ConvolvedDistribution -- convolution of two probability distributions

Functions:
    p_nonsymptomatic -- calculate probability of a case being non-symptomatic
        and unresolved (calculation engine for p_unresolved_asymptomatic and
        p_presymptomatic)
    p_unresolved_asymptomatic -- calculate probability of an asymptomatic case
        being still unresolved
    p_presymptomatic -- calculate probability of a symptomatic case being still
        pre-symptomatic
    p_missed_given_infected -- calculate probability of an infected person
        being undetected
    missed_infections -- return distribution of number of missed infections in
        group
    round_significant -- round value to specified number of significant
        digits
    print_table -- save results for CFHS COVID-19 testing placemat
    plot_group_with_test -- plot probability of undetected infection in group
        as a function of quarantine duration and number of tests
    main -- main routine when module is run directly (as opposed to being
        imported)

"""

__version__ = '0.2.1+'

