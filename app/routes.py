
from app import app
from flask import session,request,render_template,jsonify
from app.utils import getpointprevalence
from app.vizcovid.main import bokeh_vis,createtable,MissInfectData

prevalences=[0.01]
group_sizes=[10,25,50,100,250,500,1000]
days_quarantine=[14,10,7,0]
num_tests=[0,1,2,3]
prev_init=0.01  # initial point prevalence (not %)
grp_init=100    # initial group size

@app.route("/calculator",methods=["GET"])
def calculator():
    pi=prev_init
    grp=grp_init
    if request.args.get("pi") is not None:
        try:
            arg=float(request.args.get("pi"))/100
            assert arg>0
            pi=arg
        except:
            pass
    if request.args.get("grp") is not None:
        try:
            arg=int(request.args.get("grp"))
            assert arg>0
            grp=arg
        except:
            pass
    script,div,table,templateargs,initparams=bokeh_vis(group_sizes,prevalences,days_quarantine,num_tests,pi,grp)
    session["params"]=initparams.copy()
    return render_template("bokeh-bootstrap.html",bokeh_script=script,bokeh_div=div,table=table,**templateargs)

@app.route("/embed-en",methods=["GET"])
def embed_en():
    pi=prev_init
    grp=grp_init
    if request.args.get("pi") is not None:
        try:
            arg=float(request.args.get("pi"))/100
            assert arg>0
            pi=arg
        except:
            pass
    if request.args.get("grp") is not None:
        try:
            arg=int(request.args.get("grp"))
            assert arg>0
            grp=arg
        except:
            pass
    script,div,table,templateargs,initparams=bokeh_vis(group_sizes,prevalences,days_quarantine,num_tests,pi,grp,lang="en")
    session["params"]=initparams.copy()
    return render_template("bokeh-embed-en.html",bokeh_script=script,bokeh_div=div,table=table,**templateargs)

@app.route("/embed-fr",methods=["GET"])
def embed_fr():
    pi=prev_init
    grp=grp_init
    if request.args.get("pi") is not None:
        try:
            arg=float(request.args.get("pi"))/100
            assert arg>0
            pi=arg
        except:
            pass
    if request.args.get("grp") is not None:
        try:
            arg=int(request.args.get("grp"))
            assert arg>0
            grp=arg
        except:
            pass
    script,div,table,templateargs,initparams=bokeh_vis(group_sizes,prevalences,days_quarantine,num_tests,pi,grp,lang="fr")
    session["params"]=initparams.copy()
    return render_template("bokeh-embed-fr.html",bokeh_script=script,bokeh_div=div,table=table,**templateargs)

@app.route("/data",methods=["GET"])
def update_data():
    xcoords=list(range(min(days_quarantine),max(days_quarantine)+1))
    params=session["params"].copy()
    paramupdate=False
    if request.args.get("pointPrevalence") != "":
        try:
            pi=float(request.args.get("pointPrevalence"))/100
            assert pi>0
            pistr="%.2e"%(pi)
            if pistr[:4] != params["pi_base"] or int(pistr[-2:]) != params["pi_exp"]:
                params["pi_base"]=pistr[:4]
                params["pi_exp"]=int(pistr[-2:])
                paramupdate=True
        except:
            pass
    if request.args.get("groupSize") != "":
        try:
            grp=int(request.args.get("groupSize"))
            if grp != params["npers"]:
                params["npers"]=grp
                paramupdate=True
        except:
            pass
    if paramupdate:
        session["params"].update(params)
        session.modified=True
    obj0,obj1,obj2,obj3=[MissInfectData.unpackparams(params,npcr=i) for i in range(4)]
    obj0.storedata(xcoords)
    obj1.storedata(xcoords)
    obj2.storedata(xcoords)
    obj3.storedata(xcoords)
    for i in range(len(xcoords)):
        x=xcoords[i]
        if x<6:
            obj3.data[i]=0.
            obj3.strdata[i]=""
            if x<3:
                obj2.data[i]=0.
                obj2.strdata[i]=""
    tabledf=createtable([obj0.p_i],days_quarantine,num_tests,[str(x) for x in group_sizes])
    data=dict(x=xcoords,y0=obj0.data,y1=obj1.data,y2=obj2.data,y3=obj3.data,y0lab=obj0.strdata,y1lab=obj1.strdata,y2lab=obj2.strdata,y3lab=obj3.strdata)
    return jsonify({"data":data,"tabledata":tabledf.to_json(orient="records")})

@app.route("/selectprov",methods=["GET"])
def get_province_prevalence():
    if request.args.get("province") != "":
        try:
            prov=request.args.get("province")
            prev=getpointprevalence(prov)
            if prev==-1:
                raise
            assert prev>0
            prev=MissInfectData.my_rounder(prev/100,sigfig=3)
        except AssertionError:
            prev="0"
        except:
            prev=""
    return prev

