from flask import Flask

app = Flask(__name__,instance_relative_config=True)
app.secret_key='dev'
app.config.from_pyfile('config.py',silent=True)

from app import routes
