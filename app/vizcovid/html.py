
def setinitparams(prevalence,group_size):
    """ Returns JS for initialization to set initial parameters.

    The JS snippet returned will set the initial values of the input
    widgets for prevalence and group size and updates the plot title.

    Inputs:
      prevalence (str) : initial prevalence value
      group_size (str) : initial group size

    Returns:
      initscript (str) : JS snippet to insert into initialization block
    """
    initscript="""
    var previnp=document.getElementById("pointPrevalence");
    var grpinp=document.getElementById("groupSize");
    previnp.value="%PREVINP%";
    grpinp.value="%GRPINP%";
    var titleprev=document.getElementById("myTitlePrevalence");
    var titlegrp=document.getElementById("myTitleGroupSize");
    titleprev.innerHTML=previnp.value;
    titlegrp.innerHTML=grpinp.value;
    """.replace("%PREVINP%",prevalence).replace("%GRPINP%",group_size)
    return initscript


def templatetext(initscripts="",scripts=""):
    """ Collection of HTML to be used as templating arguments.

    Input (optional):
      initscripts (str) : JS with scripts to embed in document initialization
      scripts (str)     : freeform JS with scripts to embed in the final output
                          (do not use <script></script> tags)

    Returns:
      templateargs (dict/str) : snippets of HTML (values) to be inserted with keys
    """

    templateargs=dict(
    title=""" COVID-19: Missed Infection Probabilities """,

    titletext="""
    <h2 class="my-color">COVID-19 Missed Infection Probabilities</h2>
    <h4>Effect of quarantining on small groups</h4>
    """,

    leadtext="""
    <p class="lead"><!-- Sequestering individuals in small teams and testing for infection will reduce the risk of COVID-19 spread.-->
    Given <span data-toggle="tooltip" data-animation="true" data-delay="100" title="probability that a random person in the population is infected" class="hover-underline">point prevalence</span> estimates and the <span data-toggle="tooltip" data-animation="true" data-delay="100" title="a negative PCR test result for pre-symptomatic or asymptomatic cases" class="hover-underline">false negative rate</span> of current testing protocols, we examine the
    <mark>probabilities of missing at least one infected case</mark> as a function of the number of quarantining days and number of tests administered per person.</p>
    <p class="text-muted">Hover over curves on the figure to get specific point estimates and use the dropdown controls to refresh the graph. A table below reports the values for a wide range of inputs. Check out some basic <a href="#" title="Click to expand" data-toggle="modal" data-target="#assumptionsModal" class="text-reset font-weight-bolder">assumptions</a> that are behind the model.</p>
    """,

    modal1="""
    <div class="modal fade" id="assumptionsModal" tabindex="-1" role="dialog" aria-labelledby="assumptionsModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="color:#004CA3;" id="assumptionsModalLabel">Summary of assumptions</h4>
            <button type="button" class="close my-0 py-0" data-dismiss="modal" aria-label="Close">
              <i class="fas fa-window-close" style="color:#004CA3;" aria-hidden="true"></i>
            </button>
          </div>

          <div class="modal-body">
            <dl class="row">
              <dt class="col-sm-3">A.1</dt>
              <dd class="col-sm-9">
    During quarantine, individuals are kept away from each other, so they cannot cross-contaminate.
              </dd>

              <dt class="col-sm-3">A.2</dt>
              <dd class="col-sm-9">
    Individuals with COVID-19 symptoms are isolated, regardless of their PCR test results.
              </dd>

              <dt class="col-sm-3">A.3</dt>
              <dd class="col-sm-9">
    Point prevalence is constant.
              </dd>

              <dt class="col-sm-3">A.4</dt>
              <dd class="col-sm-9">
    A fraction of individuals (20%) who catch COVID-19 remain asymptomatic despite being contagious.
              </dd>

              <dt class="col-sm-3">A.5</dt>
              <dd class="col-sm-9">
    The PCR test has a constant false negative rate of 30% for asymptomatic and pre-symptomatic cases.
              </dd>

              <dt class="col-sm-3">A.6</dt>
              <dd class="col-sm-9">
    Getting a false negative in a PCR test does not increase the risk of false negative in subsequent tests.
              </dd>

              <dt class="col-sm-3">A.7</dt>
              <dd class="col-sm-9">
    Infected individuals have a pre-symptomatic phase, the duration of which follows a log-normal distribution with &mu; = 1.621 and &sigma; = 0.418.
              </dd>

              <dt class="col-sm-3">A.8</dt>
              <dd class="col-sm-9">
    The symptomatic phase for mild cases follows a gamma distribution with a mean of 10 days and a standard deviation of 3 days.
              </dd>

              <dt class="col-sm-3">A.9</dt>
              <dd class="col-sm-9">
    The infectious phase for mild symptomatic cases follows a gamma distribution with a mean of 4.59 days and a standard deviation of 1.74 days.
              </dd>

              <dt class="col-sm-3">A.10</dt>
              <dd class="col-sm-9">
    Asymptomatic cases last as long as the pre-symptomatic and infectious phases of mild symptomatic cases.
              </dd>
            </dl>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- a href="#" role="button" class="btn btn-info disabled">More information</a -->
          </div>
        </div>
      </div>
    </div>
    """,

    endtext="""
    <p>The full report on the methodology can be accessed <a href="https://diaenterprisepublic.z9.web.core.windows.net/COVID19MissedInfections/" class="text-muted">here</a>.</p>
    """,

    myscripts="""
    <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
      %INITSCRIPTS%
    });
    %SCRIPTS%
    </script>
    """.replace("%INITSCRIPTS%",initscripts).replace("%SCRIPTS%",scripts)
    )

    return templateargs

