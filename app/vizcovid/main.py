
import numpy as np
import pandas as pd
from itertools import product
from bokeh.embed import components
from app.vizcovid.plot import construct_vis
from app.vizcovid.html import setinitparams,templatetext
from app.pycovid.missed_infections import missed_infections,round_significant

class MissInfectData:
    """ Class containing modeling parameters and stored data slices.

    Instances of this class are defined for fixed values of prevalence, group size
    and number of tests. For input # days of quarantine the instance `__call__`
    method can be used to get the resulting probability of missed infection.

    e.g., `x=MissInfectData(0.01,100,2)` sets an object with 1% prevalence, a group
                                         size of 100 and 2 tests.
          `x(3)` returns probability for 3 quarantining days

    The `storedata` method can be used to attach a range of probability values
    to a particular instance.

    Class constants:
      p_a  = 0.4 : probability of an infected person being asymptomatic
      p_fn = 0.3 : probability of a false negative test result
      tinf = 950 : upper bound for integration routines in pycovid
      dt   = 0.1 : time step for integration routines in pycovid

    Instance Attributes:
      p_i (float)        : point prevalence (not %)
      npers (int)        : group size
      npcr (int)         : number of tests
      data (list/float)  : probability values calculated using `storedata`
      strdata (list/str) : string representation of `data` values in %

    Instance Methods:
      storedata  : calculate missed infection probabilities for a range of
                   quarantining days
      packparams : returns instance parameters as a modified dictionary

    Class Methods:
      unpackparams : constructs a class instance based on packed parameters

    Static Methods:
      my_rounder : get string representation of float to input significant figures
    """

    p_a=0.4
    p_fn=0.3
    tinf=950
    dt=0.1

    def __init__(self,prevalence,group_size,num_tests):
        """ Initialize MissInfectData instance.

        Inputs:
          prevalence (float) : point prevalence (not %)
          group_size (int)   : group size
          num_tests (int)    : number of tests
        """
        self.p_i=prevalence
        self.npers=group_size
        self.npcr=num_tests
        return

    def __call__(self,x):
        """ Missed infection probability for given # of quarantining days `x`. """
        return missed_infections(self.npers,self.p_i,self.p_a,self.p_fn,self.npcr,self.tinf,x,self.dt).sf(0)

    def storedata(self,xarr):
        """ Attach data to instance for a range of # of quarantining days.

        Inputs:
          xarr (list/int) : set of # quarantining days for which to store data

        Sets/updates:
          self.data (list/float)  : probabilities for each value in `xarr`
          self.strdata (list/str) : string representation (1 significant figure)
                                    for each value in `xarr` as a percentage ('x%')
        """
        self.data=[self(x) for x in xarr]
        self.strdata=[MissInfectData.my_rounder(self(x))+"%" for x in xarr]
        return

    def packparams(self):
        """ Returns instance parameters as a modified dictionary. """
        params={}
        pistr="%.2e"%(self.p_i)
        params["pi_base"]=pistr[:4]
        params["pi_exp"]=int(pistr[-2:])
        params["npers"]=self.npers
        params["npcr"]=self.npcr
        return params

    @classmethod
    def unpackparams(cls,params,npcr=None):
        """ Constructs a class instance based on packed parameters. """
        p_i=float(params["pi_base"]+"e-"+str(params["pi_exp"]))
        npers=params["npers"]
        if npcr is None:
            npcr=params["npcr"]
        return cls(p_i,npers,npcr)

    @staticmethod
    def my_rounder(x,sigfig=1):
        """ Returns a string representation of the input value scaled by 100 and
            rounded to a specified number of significant figures.

        Input:
          x (float)    : input value
          sigfig (int) : number of significant figures (optional, default=1)

        Returns:
          val (str) : rounded `x` scaled by 100 to `sigfig` significant figures
        """
        val=round_significant(100*x,sigfig)
        if val>=1 and sigfig==1:
            val=str(int(val))
        else:
            val=str(val)
        return val


def createtable(prevalences,days_quarantine,num_tests,group_sizes):
    """ Create dataframe for table output.

    The resulting dataframe will contain `3+len(group_sizes)` columns:
      - prevalence (reported as a rounded % to one significant figure)
      - quarantine days
      - # PCR tests
      - {probabilities for each group size in `group_sizes`}
        reported as a rounded % to one significant figure

    Pandas dataframe format enables easy passing to JS using:
      `tabledf.to_json(orient="records")`

    Input:
      prevalences (list/float)   : point prevalence values to tabulate (not %)
      days_quarantine (list/int) : # quarantine days to tabulate
      num_tests (list/int)       : # PCR tests to tabulate
      group_sizes (list/int)     : group sizes to tabulate

    Returns:
      tabledf (pandas dataframe) : datatable
    """
    tablecoords=list(product(prevalences,days_quarantine,num_tests))
    p_i,tc,npcr=list(zip(*tablecoords))
    tablecols=dict(p_i=list(p_i),tc=list(tc),npcr=list(npcr))
    for n in group_sizes:
        tablecols[n]=[MissInfectData.my_rounder(missed_infections(int(n),x[0],MissInfectData.p_a,MissInfectData.p_fn,x[2],MissInfectData.tinf,x[1],MissInfectData.dt).sf(0)) for x in tablecoords]
    tabledf=pd.DataFrame(tablecols)
    tabledf=tabledf[np.logical_not(np.logical_and(tabledf["npcr"]==2,tabledf["tc"]<3))]
    tabledf=tabledf[np.logical_not(np.logical_and(tabledf["npcr"]==3,tabledf["tc"]<6))]
    tabledf["p_i"]=tabledf["p_i"].apply(lambda x:MissInfectData.my_rounder(x,sigfig=3)+"%")
    return tabledf


def bokeh_vis(group_sizes,prevalences,days_quarantine,num_tests,prev_init,grp_init,lang="en"):
    """ Sets up data and objects, calls plotting routines and prepares output.

    The values of the probability of missed infection are tabulated according to
    the input parameters. This method prepares the data, calls `construct_vis` to
    generate the figure, widgets and table, and forms the necessary HTML
    components to embed in the final HTML output. The components to embed are
      - a Bokeh plot of probability of missed infection vs quarantine days, and
      - a datatable which is displayed using Bootstrap Tables.

    Inputs:
      group_sizes (list/int)     : group sizes to tabulate
      prevalences (list/float)   : point prevalence values to tabulate (not %)
      days_quarantine (list/int) : # quarantine days to tabulate
      num_tests (list/int)       : # PCR tests to tabulate
      prev_init (float)          : initial prevalence to plot/tabulate (not %)
      grp_init (int)             : initial group size to plot/tabulate

    Returns:
      script (str)            : HTML snippet for Bokeh scripts
      div (dict/str)          : HTML snippet for Bokeh divs (plot)
      table (str)             : HTML snippet for data table
      templateargs (dict/str) : HTML snippet for various template arguments
      initparams (dict/str)   : initial `MissInfectData`-style parameters
    """
    xcoords=list(np.arange(min(days_quarantine),max(days_quarantine)+1,1))
    allcoords=list(product(prevalences,group_sizes,num_tests))
    slice_data={}
    for coord in allcoords:
        strcoord="(%.0e, %d, %d)"%(coord[0],coord[1],coord[2])
        slice_data[strcoord]=MissInfectData(*coord)
        slice_data[strcoord].storedata(xcoords)
    initkeys=["(%.0e, %d, %d)"%(prev_init,grp_init,x) for x in num_tests]
    initparams=MissInfectData(prev_init,grp_init,0).packparams()

    group_sizes_str=[str(x) for x in group_sizes]
    tabledf=createtable(prevalences,days_quarantine,num_tests,group_sizes_str)

    fig,table,tablejs=construct_vis(xcoords,tabledf,group_sizes_str,slice_data,initkeys,lang)

    script,div=components({"figure":fig})

    initscript=setinitparams(MissInfectData.my_rounder(prev_init,sigfig=3),str(grp_init))
    templateargs=templatetext(initscripts=initscript,scripts=tablejs)

    return script,div,table,templateargs,initparams

