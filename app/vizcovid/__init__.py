
""" Visualization module for the probability models given in the pycovid module.

Generates HTML components of interactive visualization outputs of the probability
of missing an infected COVID-19 case in a group after quarantining and testing as
calculated from the pycovid module. Outputs include a Bokeh plot and a datatable
embedded in a Bootstrap HTML template.

General points:

 1. Interactions are provided through HTML widgets defined in the template
    outside of this module (text inputs for prevalence and group size).

 2. The custom Bokeh model `ParameterizedAjaxDataSource` has the HTML widget <id>
    tags hard-coded so any change to this necessitates a re-build of this model
    (see build branch of this repository).

 3. The data source polling occurs when widget changes are detected: this triggers
    changes in the plot and a callback is added to refresh the datatable.

 4. The two main outputs of the visualization routines are:
      - a Bokeh plot of probability of missed infection vs quarantine days, and
      - a datatable which is displayed using Bootstrap Tables.

 5. The datatable is regenerated based on the prevalence selected.

 6. A custom Bokeh model `NamedSaveTool` was written to datestamp plot downloads.

Contents:

  `vizcovid.main`:

  Classes:
    MissInfectData: contains hard-coded modeling parameters and stores instances
                    and data arrays of probability slices

  Methods:
    bokeh_vis: main driver to set up parameter ranges, populate data and
               collect objects to be added to HTML output

    createtable: generate datatable based on input parameters


  `vizcovid.plot`:

  Methods:
    construct_vis: generates plot/widget/table output objects


  `vizcovid.html`:

  Methods:
    setinitparams: generate JS to set initial prevalence and group size

    templatetext: generates the bulk of the HTML to be templated into the
                  template file

Submodules:

  `vizcovid.mymodels`: Custom Bokeh models

"""

