
import seaborn as sns
from bokeh.models.callbacks import CustomJS
from bokeh.models.formatters import FuncTickFormatter
from bokeh.models.ranges import DataRange1d
from bokeh.models.tools import HoverTool,CrosshairTool
from bokeh.plotting import figure
from app.vizcovid.mymodels import ParameterizedAjaxDataSource,NamedSaveTool

def construct_vis(xcoords,tabledf,grpsizes,slicedata,initkeys,lang):
    """ Construct Bokeh plot and datatable.

    This routine creates an interactive Bokeh plot `fig` that uses a constant
    polling datasource to update the figure with prevalence and group size according
    to the HTML widgets defined in the templates. It also creates a datatable of
    probabilities according to prevalence selected.

    Notes:
      - the keys for `slicedata` are string representations of tuples of the form
        "(prevalence, groupsize, ntests)" with
           - prevalence : given in scientific notation of the form "1e-02" (not %)
           - groupsize  : given as an integer
           - ntests     : given as an integer
        e.g., "(1e-04, 100, 2)" => 0.01% prevalence, 100 group size, 2 tests
      - the datasource is set up to poll only on widget changes (the HTML ids
        must be provided in the ParameterizedAjaxDataSource)
      - on widget change the datatable is refreshed/recalculated through the
        Bokeh websource callback adapter

    Inputs:
      xcoords (list/int)              : range of # quarantine days for plot
      tabledf (pandas dataframe)      : datatable
      grpsizes (list/str)             : group sizes
      slicedata (dict/MissInfectData) : keys="(prevalence,groupsize,ntests)"
                                        vals=instances with data/strdata attrs set
      initkeys (list/str)             : keys of `slicedata` for initial plot

    Returns:
      fig (Bokeh layout)      : Bokeh plot object (prob vs quarantine days)
      table (str)             : HTML snippet for datatable
      tablejs (str)           : JS code for datatable
    """
    palette=sns.color_palette("Set2").as_hex()

    adapter=CustomJS(code="""
    updateTable(cb_data.response.tabledata);
    return cb_data.response.data;
    """)
    dataurl="/data"
    source=ParameterizedAjaxDataSource(data_url=dataurl,method="GET",polling_interval=500,adapter=adapter)

    fig=figure(width=640,height=360,y_axis_type="log",sizing_mode="scale_width",min_height=360,tools="ypan,ywheel_zoom,reset",active_drag=None,active_scroll=None,toolbar_location="above")
    if lang=="en":
        l0=fig.line(x="x",y="y0",source=source,line_width=3,line_color=palette[-1],line_alpha=0.8,legend_label="0 tests")
    elif lang=="fr":
        l0=fig.line(x="x",y="y0",source=source,line_width=3,line_color=palette[-1],line_alpha=0.8,legend_label="0 test")
    l1=fig.line(x="x",y="y1",source=source,line_width=3,line_color=palette[0],line_alpha=0.8,legend_label="1 test")
    l2=fig.line(x="x",y="y2",source=source,line_width=3,line_color=palette[1],line_alpha=0.8,legend_label="2 tests")
    l3=fig.line(x="x",y="y3",source=source,line_width=3,line_color=palette[2],line_alpha=0.8,legend_label="3 tests")

    if lang=="en":
        fig.xaxis.axis_label="Days of quarantine"
        fig.yaxis.axis_label="Probability (%)"
    elif lang=="fr":
        fig.xaxis.axis_label="Jours de quarantaine"
        fig.yaxis.axis_label="Probabilité (%)"

    fig.xgrid.visible=False
    #fig.y_range=Range1d(start=0.000099,end=1.01,bounds=(0.000000009,1.01)) # fixed y-axis
    fig.y_range=DataRange1d(start=None,end=1.01,bounds=(0.000000009,1.01)) # autoscales y-min, fixed y-max
    roundjs="""
    var scaletick=100*tick;
    var tickpower=-Math.floor(Math.log10(scaletick));
    var tickround="";
    if (scaletick<=1) {
     tickround=scaletick.toFixed(tickpower);
    } else {
     tickround=scaletick.toString();
    }
    return tickround;
    """
    fig.yaxis.formatter=FuncTickFormatter(code=roundjs)

    mytooltip="""
    <div style="width: 175px;">
      <div>
        <span style="font-size: 14px; font-weight: bold; color: #03334E;">%LABEL1%</span>&nbsp;
        <span style="font-size: 12px;">@x{0}</span>
      </div>
      <div>
        <span style="font-size: 14px; font-weight: bold; color: %COL0%;">%LABEL2%</span>&nbsp;
        <span style="font-size: 12px;">@y0lab</span>
      </div>
      <div>
        <span style="font-size: 14px; font-weight: bold; color: %COL1%;">1 test</span>&nbsp;
        <span style="font-size: 12px;">@y1lab</span>
      </div>
      <div>
        <span style="font-size: 14px; font-weight: bold; color: %COL2%;">2 tests</span>&nbsp;
        <span style="font-size: 12px;">@y2lab</span>
      </div>
      <div>
        <span style="font-size: 14px; font-weight: bold; color: %COL3%;">3 tests</span>&nbsp;
        <span style="font-size: 12px;">@y3lab</span>
      </div>
    </div>
    """.replace("%COL0%",palette[-1]).replace("%COL1%",palette[0]).replace("%COL2%",palette[1]).replace("%COL3%",palette[2])
    if lang=="en":
        mytooltip=mytooltip.replace("%LABEL1%","Quarantine days").replace("%LABEL2%","0 tests")
    elif lang=="fr":
        mytooltip=mytooltip.replace("%LABEL1%","Jours de quarantaine").replace("%LABEL2%","0 test")
    fig.add_tools(HoverTool(tooltips=mytooltip,mode="vline",renderers=[l0]),CrosshairTool(dimensions="height",line_alpha=0.5,toggleable=False))
    fig.add_tools(NamedSaveTool())

    if lang=="en":
        fig.legend.title="# PCR Tests"
    elif lang=="fr":
        fig.legend.title="Nbre de tests de PCR"
    fig.legend.location="top_right"
    fig.legend.click_policy="hide"

    table="""
    <table id="myTable"
           class="table table-striped table-hover table-borderless table-sm"
           data-show-columns="true"
           data-show-toggle="false"
           data-show-fullscreen="true"
           data-show-export="true"
           style="font-size:80%; position:relative;">
      <thead>
        <tr>

          <th data-field="p_i" data-switchable="false">%HEADING1%</th>
          <th data-field="tc" data-switchable="false">%HEADING2%</th>
          <th data-field="npcr" data-switchable="false">%HEADING3%</th>
    """
    if lang=="en":
        table=table.replace("%HEADING1%","Prevalence").replace("%HEADING2%","Quarantine<br>days").replace("%HEADING3%","# tests")
    elif lang=="fr":
        table=table.replace("%HEADING1%","Prévalence").replace("%HEADING2%","Jours de<br>quarantaine").replace("%HEADING3%","Nbre de tests")
    for n in grpsizes:
        if lang=="en":
            table+='<th data-field="'+n+'">Prob of 1+<br>for '+n+' (%)</th>\n'
        elif lang=="fr":
            table+='<th data-field="'+n+'">Prob de 1+<br>pour '+n+' (%)</th>\n'
    table+="""
        </tr>
      </thead>
    </table>
    """
    tablejs="""
    var $table = $('#myTable');
    $(function() {
      var data = %TABLEJSON%;
      $table.bootstrapTable({
        data: data,
        exportTypes: ['csv','pdf','xml'],
        exportOptions: {
          fileName: "missed-infection-probabilities_"+myDate()
        }
      })
    })
    var myDate=function() {
      var date=new Date();
      var newdate=date.toISOString().replace(/:/g,"").split(".")[0]+"Z";
      return newdate;
    }
    var updateTable=function(data) {
      $table.bootstrapTable('load',JSON.parse(data));
    }
    """.replace("%TABLEJSON%",tabledf.to_json(orient="records"))

    return fig,table,tablejs

