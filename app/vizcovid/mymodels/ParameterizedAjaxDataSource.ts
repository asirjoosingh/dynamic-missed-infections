import {UpdateMode} from "core/enums";
import {AjaxDataSource} from "models/sources";

export class ParameterizedAjaxDataSource extends AjaxDataSource {

    elementIDsToMonitor: string[] = ['pointPrevalence','groupSize']; // Put element ID strings here
    previousElementValues: { [elementID: string]: any } = [];
    baseURL: string|null = null;

    get_data(mode: UpdateMode, max_size: number = 0, _if_modified: boolean = false): void {
        const xhr = this.prepare_request();
        if (xhr.readyState > 0) {
            xhr.addEventListener("load", () => this.do_load(xhr, mode, max_size))
            xhr.addEventListener("error", () => this.do_error(xhr))

            xhr.send()
        }
    }

    prepare_request(): XMLHttpRequest {
        if (this.baseURL === null) {
            this.baseURL = this.data_url;
        }
        let parameters = '';
        let hasNewParameters = false;
        for (let i = 0; i < this.elementIDsToMonitor.length; i++) {
            const elementID: string = this.elementIDsToMonitor[i];
            const element: HTMLInputElement = (document.getElementById(elementID) as HTMLInputElement);
            if (element === undefined || element === null) {
                console.warn('Updating data warning: Unable to locate parameter element ' + elementID);
            } else {
                if (parameters.length > 0) {
                    parameters += '&';
                } 
                parameters += elementID + '=' + element.value;
                if (this.previousElementValues[elementID] !== element.value) {
                    hasNewParameters = true;
                    this.previousElementValues[elementID] = element.value;
                }
            }
        }
        if (hasNewParameters || this.elementIDsToMonitor.length === 0) {
            this.data_url = this.baseURL + '?' + parameters;
            return super.prepare_request();
        }
        return new XMLHttpRequest();
    }
}
