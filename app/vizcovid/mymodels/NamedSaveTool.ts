import {ActionTool, ActionToolView} from "models/tools/actions/action_tool";
import * as p from "core/properties";
import {bk_tool_icon_save} from "styles/icons";

export class NamedSaveToolView extends ActionToolView {
    model: NamedSaveTool;
    doit(): void{
      var date=new Date();
      var newdate=date.toISOString().replace(/:/g,"").split(".")[0]+"Z";
      this.plot_view.save("missed-infection-probabilities_"+newdate);
    }
}

export namespace NamedSaveTool {
    export type Attrs = p.AttrsOf<Props>;
    export type Props = ActionTool.Props;
}

export interface NamedSaveTool extends NamedSaveTool.Attrs {}

export class NamedSaveTool extends ActionTool {
    properties: NamedSaveTool.Props;
    constructor(attrs?: Partial<NamedSaveTool.Attrs>) {
      super(attrs);
    }
    static init_NamedSaveTool(): void {
      this.prototype.default_view = NamedSaveToolView;
    }
    tool_name = "Save";
    icon = bk_tool_icon_save;
}
