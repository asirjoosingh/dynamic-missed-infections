
""" Custom Bokeh models for use in the vizcovid module.

Contents:

  `ParameterizedAjaxDataSource`: modifies `AjaxDataSource` model to send HTTP
                                 requests to data URL only on widget changes

  `NamedSaveTool`: customized `SaveTool` model to overwrite default Bokeh
                   plot filename with a datestamped name

"""

from .ParameterizedAjaxDataSource import ParameterizedAjaxDataSource
from .NamedSaveTool import NamedSaveTool

