from bokeh.models import AjaxDataSource

class ParameterizedAjaxDataSource(AjaxDataSource):
    """ Extended AjaxDataSource to add widget values to HTTP request (written by Matt Triff). """
    __implementation__ = "dist/lib/ParameterizedAjaxDataSource.js"
