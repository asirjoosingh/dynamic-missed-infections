from bokeh.models import Action

class NamedSaveTool(Action):
    """ Extended SaveTool to add date to a custom filename for saved plot. """
    __implementation__ = "dist/lib/NamedSaveTool.js"

