import { UpdateMode } from "core/enums";
import { AjaxDataSource } from "models/sources";
export declare class ParameterizedAjaxDataSource extends AjaxDataSource {
    elementIDsToMonitor: string[];
    previousElementValues: {
        [elementID: string]: any;
    };
    baseURL: string | null;
    get_data(mode: UpdateMode, max_size?: number, _if_modified?: boolean): void;
    prepare_request(): XMLHttpRequest;
}
