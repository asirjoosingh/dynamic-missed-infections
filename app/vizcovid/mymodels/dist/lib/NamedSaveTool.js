"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var action_tool_1 = require("models/tools/actions/action_tool");
var icons_1 = require("styles/icons");
var NamedSaveToolView = /** @class */ (function (_super) {
    __extends(NamedSaveToolView, _super);
    function NamedSaveToolView() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NamedSaveToolView.prototype.doit = function () {
        var date = new Date();
        var newdate = date.toISOString().replace(/:/g, "").split(".")[0] + "Z";
        this.plot_view.save("missed-infection-probabilities_" + newdate);
    };
    NamedSaveToolView.__name__ = "NamedSaveToolView";
    return NamedSaveToolView;
}(action_tool_1.ActionToolView));
exports.NamedSaveToolView = NamedSaveToolView;
var NamedSaveTool = /** @class */ (function (_super) {
    __extends(NamedSaveTool, _super);
    function NamedSaveTool(attrs) {
        var _this = _super.call(this, attrs) || this;
        _this.tool_name = "Save";
        _this.icon = icons_1.bk_tool_icon_save;
        return _this;
    }
    NamedSaveTool.init_NamedSaveTool = function () {
        this.prototype.default_view = NamedSaveToolView;
    };
    NamedSaveTool.__name__ = "NamedSaveTool";
    return NamedSaveTool;
}(action_tool_1.ActionTool));
exports.NamedSaveTool = NamedSaveTool;
NamedSaveTool.init_NamedSaveTool();
