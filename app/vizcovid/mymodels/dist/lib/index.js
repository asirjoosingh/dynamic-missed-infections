"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ParameterizedAjaxDataSource = require("./ParameterizedAjaxDataSource");
exports.ParameterizedAjaxDataSource = ParameterizedAjaxDataSource;
var NamedSaveTool = require("./NamedSaveTool");
exports.NamedSaveTool = NamedSaveTool;
var base_1 = require("base");
base_1.register_models(ParameterizedAjaxDataSource);
base_1.register_models(NamedSaveTool);
