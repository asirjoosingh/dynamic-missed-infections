import { ActionTool, ActionToolView } from "models/tools/actions/action_tool";
import * as p from "core/properties";
export declare class NamedSaveToolView extends ActionToolView {
    model: NamedSaveTool;
    doit(): void;
}
export declare namespace NamedSaveTool {
    type Attrs = p.AttrsOf<Props>;
    type Props = ActionTool.Props;
}
export interface NamedSaveTool extends NamedSaveTool.Attrs {
}
export declare class NamedSaveTool extends ActionTool {
    properties: NamedSaveTool.Props;
    constructor(attrs?: Partial<NamedSaveTool.Attrs>);
    static init_NamedSaveTool(): void;
    tool_name: string;
    icon: string;
}
