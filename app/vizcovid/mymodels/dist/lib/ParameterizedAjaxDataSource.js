"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var sources_1 = require("models/sources");
var ParameterizedAjaxDataSource = /** @class */ (function (_super) {
    __extends(ParameterizedAjaxDataSource, _super);
    function ParameterizedAjaxDataSource() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.elementIDsToMonitor = ['pointPrevalence', 'groupSize']; // Put element ID strings here
        _this.previousElementValues = [];
        _this.baseURL = null;
        return _this;
    }
    ParameterizedAjaxDataSource.prototype.get_data = function (mode, max_size, _if_modified) {
        var _this = this;
        if (max_size === void 0) { max_size = 0; }
        if (_if_modified === void 0) { _if_modified = false; }
        var xhr = this.prepare_request();
        if (xhr.readyState > 0) {
            xhr.addEventListener("load", function () { return _this.do_load(xhr, mode, max_size); });
            xhr.addEventListener("error", function () { return _this.do_error(xhr); });
            xhr.send();
        }
    };
    ParameterizedAjaxDataSource.prototype.prepare_request = function () {
        if (this.baseURL === null) {
            this.baseURL = this.data_url;
        }
        var parameters = '';
        var hasNewParameters = false;
        for (var i = 0; i < this.elementIDsToMonitor.length; i++) {
            var elementID = this.elementIDsToMonitor[i];
            var element = document.getElementById(elementID);
            if (element === undefined || element === null) {
                console.warn('Updating data warning: Unable to locate parameter element ' + elementID);
            }
            else {
                if (parameters.length > 0) {
                    parameters += '&';
                }
                parameters += elementID + '=' + element.value;
                if (this.previousElementValues[elementID] !== element.value) {
                    hasNewParameters = true;
                    this.previousElementValues[elementID] = element.value;
                }
            }
        }
        if (hasNewParameters || this.elementIDsToMonitor.length === 0) {
            this.data_url = this.baseURL + '?' + parameters;
            return _super.prototype.prepare_request.call(this);
        }
        return new XMLHttpRequest();
    };
    ParameterizedAjaxDataSource.__name__ = "ParameterizedAjaxDataSource";
    return ParameterizedAjaxDataSource;
}(sources_1.AjaxDataSource));
exports.ParameterizedAjaxDataSource = ParameterizedAjaxDataSource;
